package com.vamsi.tankrush;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.vamsi.tankrush.data.SceneData;
import com.vamsi.tankrush.gamestage.GameStage;

class TankRushGame extends ApplicationAdapter {

    private GameStage gameStage;

    @Override
    public void create() {
        GameAssets.loadAllAssets();
        sceneChanged(1);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (gameStage != null) {
            gameStage.act();
            gameStage.draw();
        }
    }

    private void sceneChanged(int sceneId) {
        if (gameStage != null) {
            GameStage.disposeGameStage();
        }
        SceneData.loadScene(sceneId);
        gameStage = GameStage.getInstance();
        Gdx.input.setInputProcessor(gameStage);
    }
}

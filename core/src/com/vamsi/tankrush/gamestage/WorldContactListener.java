package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.vamsi.tankrush.GameAssets;

/**
 * Created by Krishna on 21-Apr-15.
 */
class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        PhysicsActor phyActor1;
        Object actor1 = contact.getFixtureA().getUserData();
        if (actor1 != null && actor1 instanceof PhysicsActor) {
            phyActor1 = (PhysicsActor) actor1;
        } else {
            phyActor1 = null;
        }
        PhysicsActor phyActor2;
        Object actor2 = contact.getFixtureB().getUserData();
        if (actor2 != null && actor2 instanceof PhysicsActor) {
            phyActor2 = (PhysicsActor) actor2;
        } else {
            phyActor2 = null;
        }
        String name1 = phyActor1 != null ? phyActor1.getName() : "wall";
        String name2 = phyActor2 != null ? phyActor2.getName() : "wall";
        if (name1.startsWith("bullet") && name2.startsWith("bullet")) {
            processBulletBulletCollision(phyActor1, phyActor2);
        } else if (name1.startsWith("bullet")) {
            processBulletObjectCollision(phyActor1, phyActor2);
        } else if (name2.startsWith("bullet")) {
            processBulletObjectCollision(phyActor2, phyActor1);
        }
    }

    private void processBulletBulletCollision(PhysicsActor bullet1, PhysicsActor bullet2) {
        Vector2 mid = bullet1.getPosition();
        mid.add(bullet2.getPosition());
        mid.scl(0.5f);
        Group smoke = getSmoke();
        GameStage.getInstance().getTankLayer().addActor(smoke);
        smoke.setPosition(mid.x, mid.y);
        bullet1.markToRemove();
        bullet2.markToRemove();
    }

    private void processBulletObjectCollision(PhysicsActor bullet, PhysicsActor other) {
        Group smoke = getSmoke();
        GameStage.getInstance().getTankLayer().addActor(smoke);
        smoke.setPosition(bullet.getPosition().x, bullet.getPosition().y);
        if (other != null && other.getName().startsWith("tank")) {
            ((BattleTank)other).processHit();
        }
        bullet.markToRemove();
    }

    private static Group getSmoke() {
        final Group smoke = new Group();
        final Image firstSmoke = new Image(GameAssets.smoke_white_1.getTexture());
        firstSmoke.setPosition(-firstSmoke.getWidth() / 2, -firstSmoke.getHeight() / 2);
        firstSmoke.setColor(1, 1, 1, 0.5f);
        final Image secondSmoke = new Image(GameAssets.smoke_white_2.getTexture());
        secondSmoke.setPosition(-secondSmoke.getWidth() / 2, -secondSmoke.getHeight() / 2);
        secondSmoke.setColor(1, 1, 1, 0.5f);
        final Image thirdSmoke = new Image(GameAssets.smoke_white_0.getTexture());
        thirdSmoke.setPosition(-thirdSmoke.getWidth() / 2, -thirdSmoke.getHeight() / 2);
        thirdSmoke.setColor(1, 1, 1, 0.5f);

        smoke.addActor(firstSmoke);
        DelayAction delay1 = new DelayAction(0.3f);
        Action imageChange1 = new Action() {
            @Override
            public boolean act(float delta) {
                firstSmoke.remove();
                smoke.addActor(secondSmoke);
                return true;
            }
        };
        DelayAction delay2 = new DelayAction(0.3f);
        Action imageChange2 = new Action() {
            @Override
            public boolean act(float delta) {
                secondSmoke.remove();
                smoke.addActor(thirdSmoke);
                return true;
            }
        };
        DelayAction delay3 = new DelayAction(0.3f);
        Action purgeSmoke = new Action() {
            @Override
            public boolean act(float delta) {
                smoke.remove();
                return true;
            }
        };
        SequenceAction actions = new SequenceAction(delay1, imageChange1, delay2, imageChange2, delay3);
        actions.addAction(purgeSmoke);
        smoke.addAction(actions);
        return smoke;
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.vamsi.tankrush.GameAssets;
import com.vamsi.tankrush.data.Scene;
import com.vamsi.tankrush.data.SceneData;
import com.vamsi.tankrush.data.Tank;
import com.vamsi.tankrush.data.Terrain;
import com.vamsi.tankrush.gamestage.PhysicsActor.Builder;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.StaticBody;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ActorShape.circle;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ActorShape.rectangle;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ContactGroup.sand_bags;
import static com.vamsi.tankrush.gamestage.PhysicsActor.PIXEL_PER_METER;

/**
 * Created by Krishna on 16-Apr-15.
 */
public class GameStage extends Stage {

    private static GameStage instance;

    private final World world;
    private final Group tankLayer;
    private final Box2DDebugRenderer debugRenderer;

    public static GameStage getInstance() {
        if (instance == null) {
            int viewportHeight = SceneData.getInstance().getScene().getHeight();
            int viewportWidth = (Gdx.graphics.getWidth() * viewportHeight) / Gdx.graphics.getHeight();
            instance = new GameStage(viewportWidth, viewportHeight);
        }
        return instance;
    }

    private GameStage(float viewportWidth, float viewportHeight) {
        super(new FitViewport(viewportWidth, viewportHeight, new OrthographicCamera()));
        world = new World(new Vector2(0, 0), true);
        world.setContactListener(new WorldContactListener());
        float sceneWidth = SceneData.getInstance().getScene().getWidth();
        float sceneHeight = SceneData.getInstance().getScene().getHeight();
        addBg(sceneWidth, sceneHeight);
        //todo:add tank path layer here
        addOilSpills();
        addSandBags(world);
        addOilDrums(world);
        addActor(tankLayer = new Group());
        addTrees(world);
        addPhyEdges(world, sceneWidth, sceneHeight);

        PlayerTank.dispose();
        tankLayer.addActor(PlayerTank.getInstance(world));
        PlayerTank.getInstance(null).setTransform(150, 200, 0);

        for (Tank tank : SceneData.getInstance().tanks) {
            tankLayer.addActor(EnemyTank.createInstance(world, tank));
        }

        debugRenderer = new Box2DDebugRenderer();
    }

    private void addBg(float width, float height) {
        Scene scene = SceneData.getInstance().scenes.get(0);
        Texture backgroundTexture = GameAssets.valueOf(scene.background).getTexture();
        Table backgroundTable = new Table();
        backgroundTable.setSize(width, height);
        for (float i = 0; i < scene.height; i++) {
            for (float j = 0; j < scene.width; j++) {
                backgroundTable.add(new Image(backgroundTexture));
            }
            backgroundTable.row();
        }
        addActor(backgroundTable);
        backgroundTable.addListener(getTankController());
    }

    private InputListener getTankController() {
        return new InputListener() {
            private float initTouchX;
            private float initTouchY;
            private final int RADIUS = 40;
            private final int VELOCITY = 2;
            private boolean dragInit = false;

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (pointer == 0) {
                    this.initTouchX = x;
                    this.initTouchY = y;
                }
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (pointer == 0) {
                    dragInit = true;
                    float diffX = x - initTouchX;
                    float diffY = y - initTouchY;
                    if (diffX * diffX + diffY * diffY > RADIUS * RADIUS) {
                        double ratio = Math.sqrt((diffX * diffX + diffY * diffY) / (RADIUS * RADIUS));
                        diffX = (float) (diffX / ratio);
                        diffY = (float) (diffY / ratio);
                    }
                    float xRatio = diffX / RADIUS;
                    float yRatio = diffY / RADIUS;
                    PlayerTank.getInstance(null).instruct(VELOCITY * xRatio, VELOCITY * yRatio);
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (pointer == 1 || (pointer == 0 && !dragInit)) {
                    PlayerTank.getInstance(null).fire(x, y);
                } else {
                    dragInit = false;
                    PlayerTank.getInstance(null).resetInstruction();
                }
            }
        };
    }

    private void addOilSpills() {
        for (Terrain terrain : SceneData.getInstance().terrain) {
            if (terrain.type.equals("oil_spill")) {
                Image oilSpill = new Image(GameAssets.oil_spill.getTexture());
                oilSpill.setPosition(terrain.x - oilSpill.getWidth() / 2, terrain.y - oilSpill.getHeight() / 2);
                this.addActor(oilSpill);
                oilSpill.setTouchable(Touchable.disabled);
            }
        }
    }

    private void addSandBags(World world) {
        for (Terrain terrain : SceneData.getInstance().terrain) {
            if (terrain.type.startsWith("sand_bag_")) {
                Texture sandBagTexture = GameAssets.valueOf(terrain.type).getTexture();
                Image image = new Image(sandBagTexture);
                Builder builder = new Builder().setActor(image).setWorld(world)
                        .setBodyType(StaticBody).setActorShape(rectangle)
                        .setContactGroup(sand_bags).setName("sand_bag");
                PhysicsActor actor = new PhysicsActor(builder);
                addActor(actor);
                actor.setTransform(terrain.x, terrain.y, terrain.angle);
                actor.setTouchable(Touchable.disabled);
            }
        }
    }

    private void addOilDrums(World world) {
        for (Terrain terrain : SceneData.getInstance().terrain) {
            if (terrain.type.startsWith("barrel_")) {
                Texture barrelTexture = GameAssets.valueOf(terrain.type).getTexture();
                Image barrel = new Image(barrelTexture);
                PhysicsActor actor;
                Builder builder = new Builder().setActor(barrel).setWorld(world).setBodyType(DynamicBody)
                        .setName("oil_drum");
                if (terrain.type.endsWith("_up")) {
                    builder.setActorShape(circle);
                } else {
                    builder.setActorShape(rectangle);
                }
                actor = new PhysicsActor(builder);
                actor.setTransform(terrain.x, terrain.y, terrain.angle);
                addActor(actor);
                actor.setTouchable(Touchable.disabled);
            }
        }
    }

    private void addTrees(World world) {
        for (Terrain terrain : SceneData.getInstance().terrain) {
            if (terrain.type.startsWith("tree_")) {
                Texture treeTexture = GameAssets.valueOf(terrain.type).getTexture();
                Image tree = new Image(treeTexture);
                Builder builder = new Builder().setActor(tree).setWorld(world).
                        setActorShape(circle).setBodyType(StaticBody).setScale(0.5f)
                        .setName("tree");
                PhysicsActor actor = new PhysicsActor(builder);
                actor.setTransform(terrain.x, terrain.y, terrain.angle);
                addActor(actor);
                actor.setTouchable(Touchable.disabled);
            }
        }
    }

    private void addPhyEdges(World world, float width, float height) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = StaticBody;
        addPhyEdge(world, bodyDef, 0, 0, width, 0);
        addPhyEdge(world, bodyDef, width, 0, width, height);
        addPhyEdge(world, bodyDef, 0, height, width, height);
        addPhyEdge(world, bodyDef, 0, 0, 0, height);
    }

    private void addPhyEdge(World world, BodyDef bodyDef, float startX, float startY, float endX, float endY) {
        float scaledStartX = startX / PIXEL_PER_METER;
        float scaledStartY = startY / PIXEL_PER_METER;
        float scaledEndX = endX / PIXEL_PER_METER;
        float scaledEndY = endY / PIXEL_PER_METER;
        FixtureDef fixtureDef = new FixtureDef();
        EdgeShape bottomEdgeShape = new EdgeShape();
        bottomEdgeShape.set(scaledStartX, scaledStartY, scaledEndX, scaledEndY);
        fixtureDef.shape = bottomEdgeShape;
        fixtureDef.filter.categoryBits = PhysicsActor.ContactGroup.rest.category;
        fixtureDef.filter.maskBits = PhysicsActor.ContactGroup.rest.mask;
        Body bottomBody = world.createBody(bodyDef);
        bottomBody.createFixture(fixtureDef);
        bottomEdgeShape.dispose();
    }


    @Override
    public void act(float delta) {
        super.act(delta);
        PhysicsActor.removeObsoleteActors();
        world.step(delta, 6, 2);
    }

    @Override
    public void draw() {
        super.draw();
        Matrix4 debugMatrix = getBatch().getProjectionMatrix().cpy()
                .scale(PhysicsActor.PIXEL_PER_METER, PhysicsActor.PIXEL_PER_METER, 0);
        debugRenderer.render(world, debugMatrix);
    }

    @Override
    public void dispose() {
        super.dispose();
        world.dispose();
        debugRenderer.dispose();
    }

    public static void disposeGameStage() {
        instance.dispose();
        instance = null;
    }

    public Group getTankLayer() {
        return tankLayer;
    }

}

package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.vamsi.tankrush.data.Tank;
import com.vamsi.tankrush.data.TankMovement;

import java.util.ArrayDeque;
import java.util.Queue;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ActorShape.rectangle;

/**
 * Created by Krishna on 23-Apr-15.
 */
public class EnemyTank extends BattleTank {

    private final Queue<TankMovement> movements;
    private boolean isAlive = true;
    private final PathFinder pathFinder;

    public static EnemyTank createInstance(World world, Tank tankData) {
        Builder builder = new Builder();
        builder.setTankData(tankData).setWorld(world)
                .setActorShape(rectangle).setBodyType(DynamicBody).setName("tank_enemy");
        return new EnemyTank(builder, world);
    }

    private EnemyTank(Builder builder, World world) {
        super(builder);
        pathFinder = new PathFinder(world, getRadius());
        this.movements = builder.movements;
        setTransform(movements.peek().x, movements.peek().y, 0);

        DelayAction delayAction = new DelayAction(builder.tankData.firingInterval);
        FaceHeroTankAction faceHero = new FaceHeroTankAction(this);
        Action fire = new Action() {
            @Override
            public boolean act(float delta) {
                fire(PlayerTank.getInstance(null).getPosition().x, PlayerTank.getInstance(null).getPosition().y);
                return true;
            }
        };
        SequenceAction sequenceAction = new SequenceAction(delayAction, faceHero, fire);
        RepeatAction repeatAction = new RepeatAction();
        repeatAction.setAction(sequenceAction);
        repeatAction.setCount(RepeatAction.FOREVER);

        this.addAction(repeatAction);
        this.health = 2;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isAlive) {
            TankMovement tankMovement = movements.peek();
            Vector2 direction = pathFinder.getDirection(tankMovement.x, tankMovement.y,
                    getPosition().x, getPosition().y);

            if (direction.len() < 0.00001) {
                TankMovement tankMovement1 = movements.poll();
                movements.add(tankMovement1);
                return;
            }
            Vector2 velocity = new Vector2(direction);
            if (velocity.len() > 0) {
                float tankAngle = this.getAngle() + 90;
                float velocityAngle = velocity.angle();
                float theta = velocityAngle - tankAngle;
                float cos = (float) Math.cos(Math.toRadians(theta));
                float sin = (float) Math.sin(Math.toRadians(theta));
                if (cos > 0.999) {
                    body.setLinearVelocity(velocity);
                    body.setAngularVelocity(0);
                } else if (cos > 0.99) {
                    body.setLinearVelocity(velocity);
                    body.setAngularVelocity(sin > 0 ? 0.3f : -0.3f);
                } else {
                    body.setAngularVelocity(sin > 0 ? 1f : -1f);
                    body.setLinearVelocity(0, 0);
                }
            } else {
                body.setLinearVelocity(0, 0);
                body.setAngularVelocity(0);
            }
        }
    }

    @Override
    public boolean canFire() {
        return true;
    }

    @Override
    protected void die() {
        isAlive = false;
        this.clearActions();
        DeadTankSmoke smoke = new DeadTankSmoke();
        ((Group) actor).addActor(smoke);
        smoke.setPosition(actor.getWidth() / 2, actor.getHeight() / 2);
    }

    public static class Builder extends PhysicsActor.Builder {
        Tank tankData;
        Queue<TankMovement> movements;

        public Builder setTankData(Tank tankData) {
            this.tankData = tankData;
            this.movements = new ArrayDeque<TankMovement>();
            this.movements.addAll(tankData.getTankMovements());
            return this;
        }
    }
}

package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 19-Apr-15.
 */
public class PhysicsActor extends Group {
    final public static int PIXEL_PER_METER = 44;
    private static final List<PhysicsActor> obsoleteActors = new ArrayList<PhysicsActor>();
    final Actor actor;
    final Body body;
    private final ActorShape actorShape;

    public PhysicsActor(Builder builder) {
        this.actor = builder.actor;
        actor.setName(builder.name);
        actor.setOrigin(actor.getWidth() / 2, actor.getHeight() / 2);
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = builder.bodyType;
        bodyDef.bullet = builder.isBullet;
        body = builder.world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        Shape shape;
        this.actorShape = builder.actorShape;
        switch (builder.actorShape) {
            case rectangle:
                shape = new PolygonShape();
                ((PolygonShape) shape).setAsBox(
                        builder.scale * actor.getWidth() / (2 * PIXEL_PER_METER),
                        builder.scale * actor.getHeight() / (2 * PIXEL_PER_METER));
                break;
            case circle:
                shape = new CircleShape();
                shape.setRadius(builder.scale * actor.getHeight() / (2 * PIXEL_PER_METER));
                break;
            default:
                shape = null;
        }
        fixtureDef.shape = shape;
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = builder.contactGroup.category;
        fixtureDef.filter.maskBits = builder.contactGroup.mask;
        body.createFixture(fixtureDef).setUserData(this);
        this.addActor(actor);
        shape.dispose();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (body.isActive()) {
            float x = (body.getPosition().x * PIXEL_PER_METER) - actor.getWidth() / 2;
            float y = (body.getPosition().y * PIXEL_PER_METER) - actor.getHeight() / 2;
            actor.setPosition(x, y);
            actor.setRotation((float) Math.toDegrees(body.getAngle()));
        }
    }

    /**
     * @param x     the x-cooordinate of center of actor in pixels
     * @param y     the y-coordinate of the center of actor in pixels
     * @param angle in degrees
     */
    public void setTransform(float x, float y, float angle) {
        body.setTransform(x / PIXEL_PER_METER, y / PIXEL_PER_METER, (float) Math.toRadians(angle));
        actor.setPosition(x - actor.getWidth() / 2, y - actor.getHeight() / 2);
        actor.setRotation(angle);
    }

    /**
     * The mid point of the shape
     *
     * @return in pixels
     */
    public Vector2 getPosition() {
        Vector2 pos = new Vector2(body.getPosition());
        pos.scl(PIXEL_PER_METER);
        return pos;
    }

    public float getAngle() {
        return (float) Math.toDegrees(body.getAngle());
    }

    public float getRadius() {
        switch (actorShape) {
            case rectangle:
                return Math.min(actor.getHeight() / 2, actor.getWidth() / 2);
            case circle:
                return body.getFixtureList().get(0).getShape().getRadius();
            default:
                return 0;
        }
    }

    /**
     * @param vx in pixels
     * @param vy in pixels
     */
    void setLinearVelocity(float vx, float vy) {
        body.setLinearVelocity(vx / PIXEL_PER_METER, vy / PIXEL_PER_METER);
    }

    public String getName() {
        return actor.getName();
    }

    public void markToRemove() {
        PhysicsActor.obsoleteActors.add(this);
    }

    public static void removeObsoleteActors() {
        for (PhysicsActor actor : obsoleteActors) {
            actor.remove();
        }
        obsoleteActors.clear();
    }

    @Override
    public boolean remove() {
        body.getWorld().destroyBody(body);
        return super.remove();
    }

    public enum ActorShape {
        rectangle,
        circle
    }

    public enum ContactGroup {
        bullets(1, 1, 3),
        sand_bags(2, 2, 3),
        rest(3, 1, 2, 3);

        public final short category;
        public short mask;

        ContactGroup(int number, int... ints) {
            category = (short) (1 << number);
            mask = 0;
            for (int maskBit : ints) {
                mask = (short) (mask | (1 << maskBit));
            }
        }
    }

    public static class Builder {
        private String name;
        public Actor actor;
        private World world;
        private ActorShape actorShape = ActorShape.rectangle;
        private BodyDef.BodyType bodyType = BodyDef.BodyType.DynamicBody;
        private float scale = 1f;
        private boolean isBullet = false;
        private ContactGroup contactGroup = ContactGroup.rest;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setActor(Actor actor) {
            this.actor = actor;
            return this;
        }

        public Builder setWorld(World world) {
            this.world = world;
            return this;
        }

        public Builder setActorShape(ActorShape actorShape) {
            this.actorShape = actorShape;
            return this;
        }

        public Builder setBodyType(BodyDef.BodyType bodyType) {
            this.bodyType = bodyType;
            return this;
        }

        public Builder setScale(float scale) {
            this.scale = scale;
            return this;
        }

        public Builder setBullet(boolean isBullet) {
            this.isBullet = isBullet;
            return this;
        }

        public Builder setContactGroup(ContactGroup contactGroup) {
            this.contactGroup = contactGroup;
            return this;
        }
    }

}

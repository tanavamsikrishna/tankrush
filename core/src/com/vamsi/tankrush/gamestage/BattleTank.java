package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.vamsi.tankrush.GameAssets;
import com.vamsi.tankrush.interfaces.TankControls;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ActorShape.rectangle;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ContactGroup.bullets;

/**
 * Created by Krishna on 23-Apr-15.
 */
public abstract class BattleTank extends PhysicsActor implements TankControls {

    private static final Vector2 gunOffset;
    private static final Vector2 gunTipReference;
    final private static float BULLET_SPEED = 6 * PIXEL_PER_METER;//in metres per second
    public final TankActor tankActor;
    int health;

    static {
        gunOffset = new Vector2(-GameAssets.gun_blue.getTexture().getWidth() / 2f, 8f);
        gunTipReference = new Vector2(0f, gunOffset.y + GameAssets.gun_blue.getTexture().getHeight());
    }

    BattleTank(Builder builder) {
        super(builder.setActor(new TankActor()));
        tankActor = (TankActor) builder.actor;
        this.setTouchable(Touchable.disabled);
    }

    @Override
    public void instruct(float xSpeed, float ySpeed) {

    }

    @Override
    public void resetInstruction() {

    }

    abstract protected boolean canFire();

    @Override
    public void fire(float x, float y) {
        if (!canFire()) return;
        Vector2 currentGunTipPos = new Vector2(gunTipReference);
        Image bulletImage = new Image(GameAssets.bullet_blue.getTexture());
        currentGunTipPos.add(0, bulletImage.getHeight() / 2);

        Vector2 aimLoc = new Vector2(x, y);
        aimLoc.sub(this.getPosition());
        float gunAngle = (aimLoc.angle() - 90);
        tankActor.setGunAngle(gunAngle - this.getAngle());
        currentGunTipPos.rotate(gunAngle);

        currentGunTipPos.add(this.getPosition());

        Builder builder = new Builder().setActor(bulletImage).setWorld(body.getWorld())
                .setActorShape(rectangle).setBodyType(DynamicBody)
                .setBullet(true).setContactGroup(bullets).setName("bullet");
        PhysicsActor bulletPhyActor = new PhysicsActor(builder);
        bulletPhyActor.setTransform(currentGunTipPos.x, currentGunTipPos.y, gunAngle);
        Vector2 bulletSpeed = new Vector2(0, BULLET_SPEED);
        bulletSpeed.rotate(gunAngle);
        bulletPhyActor.setLinearVelocity(bulletSpeed.x, bulletSpeed.y);

        GameStage.getInstance().getTankLayer().addActor(bulletPhyActor);

        Actor smoke = getSmoke();
        GameStage.getInstance().getTankLayer().addActor(smoke);
        smoke.setPosition(currentGunTipPos.x - smoke.getWidth() / 2, currentGunTipPos.y - smoke.getHeight() / 2);
    }

    private static Actor getSmoke() {
        final Image smoke = new Image(GameAssets.smoke_white_2.getTexture());
        smoke.setColor(1, 1, 1, 0.5f);
        DelayAction wait1 = new DelayAction(0.2f);
        Action removeSmoke = new Action() {
            @Override
            public boolean act(float delta) {
                smoke.remove();
                return true;
            }
        };
        SequenceAction actions = new SequenceAction(wait1, removeSmoke);
        smoke.addAction(actions);
        return smoke;
    }

    public void processHit() {
        health--;
        if(health == 0) {
            die();
        }
    }

    protected abstract void die();

    public static class TankActor extends Group {
        public final Image gun;

        public TankActor() {
            Image chassis = new Image(GameAssets.blue_tank.getTexture());
            gun = new Image(GameAssets.gun_blue.getTexture());
            this.addActor(chassis);
            this.addActor(gun);
            gun.setPosition((chassis.getWidth() / 2) + gunOffset.x, (chassis.getHeight() / 2) + gunOffset.y);
            gun.setOrigin(-gunOffset.x, -gunOffset.y);
            this.setSize(chassis.getWidth(), chassis.getHeight());
        }

        public void setGunAngle(float angle) {
            gun.setRotation(angle);
        }
    }
}

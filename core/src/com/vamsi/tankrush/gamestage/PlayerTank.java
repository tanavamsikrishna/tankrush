package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.vamsi.tankrush.gamestage.PhysicsActor.ActorShape.rectangle;

/**
 * Created by Krishna on 20-Apr-15.
 */
public class PlayerTank extends BattleTank {

    private static PlayerTank instance;

    /**
     * @param world can be null when an instance already exists
     * @return instance
     */
    public static PlayerTank getInstance(World world) {
        if (instance == null) {
            Builder builder = new Builder().setWorld(world).setActorShape(rectangle)
                    .setBodyType(DynamicBody).setName("tank_player");
            instance = new PlayerTank(builder);
        }
        return instance;
    }

    private PlayerTank(Builder builder) {
        super(builder);
        this.health = 6;
    }

    private float xSpeed;
    private float ySpeed;

    @Override
    public void instruct(float xSpeed, float ySpeed) {
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void resetInstruction() {
        this.xSpeed = 0;
        this.ySpeed = 0;
    }

    @Override
    public boolean canFire() {
        long currentTime = System.currentTimeMillis();
        if (currentTime < prevFireTime + 250) {
            return false;
        } else {
            prevFireTime = currentTime;
            return true;
        }

    }

    @Override
    protected void die() {

    }

    private long prevFireTime = 0;

    @Override
    public void act(float delta) {
        super.act(delta);
        Vector2 velocity = new Vector2(xSpeed, ySpeed);
        if (velocity.len() > 0) {
            float tankAngle = this.getAngle() + 90;
            float velocityAngle = velocity.angle();
            float theta = velocityAngle - tankAngle;
            float cos = (float) Math.cos(Math.toRadians(theta));
            float sin = (float) Math.sin(Math.toRadians(theta));
            if (cos < 0) {
                tankAngle += 180;
                theta -= 180;
                sin *= -1;
                cos *= -1;
            }
            if (cos > 0.999) {
                body.setAngularVelocity(0);
            } else if (cos > 0.99) {
                body.setAngularVelocity(sin > 0 ? 0.3f : -0.3f);
            } else {
                body.setAngularVelocity(sin > 0 ? 1f : -1f);
            }
            velocity.setAngle(tankAngle);
            body.setLinearVelocity(velocity);
        } else {
            body.setLinearVelocity(0, 0);
            body.setAngularVelocity(0);
        }

    }

    public static void dispose() {
        instance = null;
    }
}

package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.vamsi.tankrush.data.SceneData;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Krishna on 23-Apr-15.
 */
class PathFinder {
    final private int PIXEL_ROUND_OFF = 20;
    private int[][] matrix;
    private final int matrixWidth;
    private final int matrixHeight;
    private final int widthInPixels;
    private final int heightInPixels;
    private final World world;
    private final float radiusInPixels;
    private int matrixGoalX = -1;
    private int matrixGoalY = -1;

    public PathFinder(World world, float radiusInPixels) {
        this.widthInPixels = SceneData.getInstance().getScene().getWidth();
        this.heightInPixels = SceneData.getInstance().getScene().getHeight();
        matrixWidth = Math.round(widthInPixels / PIXEL_ROUND_OFF) + 1;
        matrixHeight = Math.round(heightInPixels / PIXEL_ROUND_OFF) + 1;
        this.world = world;
        this.radiusInPixels = radiusInPixels;
        reset();
    }

    /**
     * All function parameters in pixels
     */
    public Vector2 getDirection(float goalX, float goalY, float posX, float posY) {
        calculate(Math.round(goalX / PIXEL_ROUND_OFF), Math.round(goalY / PIXEL_ROUND_OFF));
        int matrixPosX = Math.round(posX / PIXEL_ROUND_OFF);
        int matrixPosY = Math.round(posY / PIXEL_ROUND_OFF);
        Vector2 matrixPos = new Vector2(matrixPosX, matrixPosY);
        int dist = matrix[matrixPosX][matrixPosY];
        Vector2 direction = new Vector2();
        for (Tuple tuple : getNeighbours(matrixPosX, matrixPosY)) {
            if (matrix[tuple.x][tuple.y] == dist - 1) {
                direction.add(tuple.x, tuple.y);
                direction.sub(matrixPos);
            }
        }
        if (direction.len() > 0.0001) {
            direction.scl(1 / direction.len());
        }
        return direction;
    }

    private void calculate(int matrixGoalX, int matrixGoalY) {
        if (this.matrixGoalX != matrixGoalX || this.matrixGoalY != matrixGoalY) {
            reset();
            this.matrixGoalX = matrixGoalX;
            this.matrixGoalY = matrixGoalY;
            matrix[matrixGoalX][matrixGoalY] = 0;
            Queue<Tuple> neighbourQueue = new LinkedList<Tuple>();
            neighbourQueue.add(new Tuple(matrixGoalX, matrixGoalY));
            while (!neighbourQueue.isEmpty()) {
                Tuple currentPos = neighbourQueue.poll();
                for (Tuple tuple : getNeighbours(currentPos.x, currentPos.y)) {
                    if (matrix[tuple.x][tuple.y] > matrix[currentPos.x][currentPos.y] + 1) {
                        matrix[tuple.x][tuple.y] = matrix[currentPos.x][currentPos.y] + 1;
                        neighbourQueue.add(tuple);
                    }
                }
            }
        }
    }

    private Collection<Tuple> getNeighbours(int x, int y) {
        List<Tuple> allNeighbours = new LinkedList<Tuple>();
        allNeighbours.add(new Tuple(x - 1, y - 1));
        allNeighbours.add(new Tuple(x - 1, y));
        allNeighbours.add(new Tuple(x - 1, y + 1));
        allNeighbours.add(new Tuple(x, y - 1));
        allNeighbours.add(new Tuple(x, y + 1));
        allNeighbours.add(new Tuple(x + 1, y - 1));
        allNeighbours.add(new Tuple(x + 1, y));
        allNeighbours.add(new Tuple(x + 1, y + 1));
        List<Tuple> unRealNeighbours = new LinkedList<Tuple>();
        for (Tuple tuple : allNeighbours) {
            if (tuple.x < 0) {
                unRealNeighbours.add(tuple);
            }
            if (tuple.y < 0) {
                unRealNeighbours.add(tuple);
            }
            if (tuple.x >= matrixWidth) {
                unRealNeighbours.add(tuple);
            }
            if (tuple.y >= matrixHeight) {
                unRealNeighbours.add(tuple);
            }
        }
        allNeighbours.removeAll(unRealNeighbours);
        Array<Body> worldBodies = new Array<Body>();
        world.getBodies(worldBodies);

        for (Tuple tuple : allNeighbours) {
            Vector2 pointPos = new Vector2(tuple.x, tuple.y).scl(PIXEL_ROUND_OFF);
            if (pointPos.x < radiusInPixels || pointPos.y < radiusInPixels ||
                    pointPos.x + radiusInPixels > widthInPixels || pointPos.y + radiusInPixels > heightInPixels) {
                unRealNeighbours.add(tuple);
            } else {
                for (Body body : worldBodies) {
                    Object userData = body.getFixtureList().get(0).getUserData();
                    if (userData != null && userData instanceof PhysicsActor) {
                        PhysicsActor actor = (PhysicsActor) userData;
                        if (actor.getName().startsWith("tank_") ||
                                actor.getName().equals("bullet")) {
                            continue;
                        }
                        Vector2 bodyPos = actor.getPosition();
                        if (bodyPos.sub(pointPos).len() < actor.getRadius() + radiusInPixels) {
                            unRealNeighbours.add(tuple);
                            break;
                        }
                    }
                }
            }
        }
        allNeighbours.removeAll(unRealNeighbours);
        return allNeighbours;
    }

    private void reset() {
        matrix = new int[matrixWidth][matrixHeight];
        for (int i = 0; i < matrixWidth; i++) {
            for (int j = 0; j < matrixHeight; j++) {
                matrix[i][j] = Integer.MAX_VALUE;
            }
        }
    }

    public static class Tuple {
        public final int x;
        public final int y;

        private Tuple(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else if (!(obj instanceof Tuple)) {
                return false;
            } else {
                Tuple tuple = (Tuple) obj;
                return tuple.x == x && tuple.y == y;
            }
        }
    }

    public Actor matrixView() {
        Pixmap pixmap = new Pixmap(widthInPixels, heightInPixels, Pixmap.Format.RGB888);
        for (int i = 0; i < widthInPixels; i += 1) {
            int matrixWidth = Math.round(i / PIXEL_ROUND_OFF);
            for (int j = 0; j < heightInPixels; j += 1) {
                int matrixHeight = Math.round(j / PIXEL_ROUND_OFF);
                float color = (matrix[matrixWidth][matrixHeight] / 50f) % 1;
                int colorInt = (int) (color * 255);
                pixmap.setColor(Color.rgb888(colorInt, colorInt, colorInt));
                pixmap.drawPixel(i, heightInPixels - j);
            }
        }
        return new Image(new Texture(pixmap));
    }
}

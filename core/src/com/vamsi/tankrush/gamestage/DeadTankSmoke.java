package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.vamsi.tankrush.GameAssets;

/**
 * Created by Krishna on 06-May-15.
 */
class DeadTankSmoke extends Group {

    private static final float ANGULAR_SPEED = 25;
    private static final float SMOKE_INIT_DIST = 5;
    private static final float SMOKE_LIFE_SPAN = 10;

    public DeadTankSmoke() {
        DelayAction smokeCreateDelay = new DelayAction(2f);
        Action produceSmoke = new Action() {
            @Override
            public boolean act(float delta) {
                final Image smoke = new Image(GameAssets.smoke_grey_0.getTexture());
                DeadTankSmoke.this.addActorAt(0, smoke);
                SmokeMovementAction smokeMovementAction = new SmokeMovementAction();
                Action killSmole = new Action() {
                    @Override
                    public boolean act(float delta) {
                        smoke.remove();
                        return true;
                    }
                };
                SequenceAction action = new SequenceAction(smokeMovementAction, killSmole);
                smoke.addAction(action);
                return true;
            }
        };
        SequenceAction action1 = new SequenceAction(smokeCreateDelay, produceSmoke);
        RepeatAction action2 = new RepeatAction();
        action2.setAction(action1);
        action2.setCount(RepeatAction.FOREVER);
        this.addAction(action2);

    }

    private class SmokeMovementAction extends Action {
        float time = 0;
        final Vector2 pos = new Vector2(0, -SMOKE_INIT_DIST);

        @Override
        public boolean act(float delta) {
            if (time > SMOKE_LIFE_SPAN) {
                return true;
            } else {
                pos.rotate(ANGULAR_SPEED * delta);
                pos.scl(SMOKE_INIT_DIST / pos.len());
                pos.scl(2 + (time / SMOKE_LIFE_SPAN));
                getActor().setPosition(pos.x - (getActor().getWidth() / 2), pos.y - (getActor().getHeight() / 2));
                getActor().setScale((1.5f + (time / SMOKE_LIFE_SPAN)) / 2f);
                if (time < 0.2 * SMOKE_LIFE_SPAN) {
                    getActor().setColor(1, 1, 1, time / (0.2f * SMOKE_LIFE_SPAN));
                } else if (time > 0.8 * SMOKE_LIFE_SPAN) {
                    getActor().setColor(1, 1, 1, 1 - ((time - 0.8f * SMOKE_LIFE_SPAN) / (0.2f * SMOKE_LIFE_SPAN)));
                }
                time += delta;
                return false;
            }
        }
    }
}

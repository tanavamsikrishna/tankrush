package com.vamsi.tankrush.gamestage;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.vamsi.tankrush.gamestage.BattleTank;
import com.vamsi.tankrush.gamestage.PlayerTank;

/**
 * Created by Krishna on 24-Apr-15.
 */
class FaceHeroTankAction extends Action {
    private final BattleTank actor;

    public FaceHeroTankAction(BattleTank actor) {
        this.actor = actor;
    }

    @Override
    public boolean act(float delta) {
        Vector2 heroTankPos = PlayerTank.getInstance(null).getPosition();
        Vector2 bodyPos = actor.getPosition();
        float heroDirection = heroTankPos.sub(bodyPos).angle();
        float tankAngle = actor.getAngle() + 90;
        float gunAngle = tankAngle + actor.tankActor.gun.getRotation();
        float theta = heroDirection - gunAngle;
        if (Math.cos(Math.toRadians(theta)) > 0.999) {
            actor.tankActor.gun.setRotation(heroDirection - tankAngle);
            return true;
        } else {
            float angleDisplacement = Math.sin(Math.toRadians(theta)) > 0 ? 1 : -1;
            angleDisplacement *= delta * 90;
            actor.tankActor.gun.setRotation(actor.tankActor.gun.getRotation() + angleDisplacement);
            return false;
        }
    }
}

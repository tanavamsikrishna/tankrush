package com.vamsi.tankrush;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Krishna on 17-Apr-15.
 */

public enum GameAssets {
    bullet_blue("bullets/bulletBlue_outline.png", Texture.class),

    direction_left("controller/direction_left.png", Texture.class),
    direction_right("controller/direction_right.png", Texture.class),
    direction_top("controller/direction_top.png", Texture.class),
    direction_bottom("controller/direction_bottom.png", Texture.class),
    round_controller("controller/round_controller.png", Texture.class),

    dirt("background/dirt.png", Texture.class),
    grass("background/grass.png", Texture.class),
    sand("background/sand.png", Texture.class),
    popup_background("background/popup_background.png", Texture.class),

    smoke_grey_0("smoke/smokeGrey0.png", Texture.class),
    smoke_grey_5("smoke/smokeGrey5.png", Texture.class),
    smoke_grey_2("smoke/smokeGrey2.png", Texture.class),
    smoke_grey_3("smoke/smokeGrey3.png", Texture.class),
    smoke_white_0("smoke/smokeWhite0.png", Texture.class),
    smoke_white_1("smoke/smokeWhite1.png", Texture.class),
    smoke_white_2("smoke/smokeWhite2.png", Texture.class),

    blue_tank("tank/tankBlue.png", Texture.class),
    gun_blue("tank/barrelBlue_outline.png", Texture.class),

    tree_small("obstacles/treeSmall.png", Texture.class),
    tree_large("obstacles/treeLarge.png", Texture.class),
    barrel_green_side("obstacles/barrelGreen_side.png", Texture.class),
    barrel_green_side_damaged("obstacles/barrelGreen_side_damaged.png", Texture.class),
    barrel_green_up("obstacles/barrelGreen_up.png", Texture.class),
    barrel_grey_side_damaged("obstacles/barrelGrey_sde_rust.png", Texture.class),
    barrel_grey_side("obstacles/barrelGrey_side.png", Texture.class),
    barrel_grey_up("obstacles/barrelGrey_up.png", Texture.class),
    barrel_red_side("obstacles/barrelRed_side.png", Texture.class),
    barrel_red_up("obstacles/barrelRed_up.png", Texture.class),
    sand_bag_grey("obstacles/sandbagGrey.png", Texture.class),
    sand_bag_brown("obstacles/sandbagBrown.png", Texture.class),
    oil_spill("obstacles/oil.png", Texture.class);


    private static final AssetManager assetManager = new AssetManager();
    private final String filePath;
    private final Class clazz;

    GameAssets(String filePath, Class clazz) {
        this.filePath = filePath;
        this.clazz = clazz;
    }

    public static void loadAllAssets() {
        for (GameAssets gameAsset : GameAssets.values()) {
            gameAsset.load();
        }
        assetManager.finishLoading();
    }

    private void load() {
        assetManager.load(filePath, clazz);
    }

    public Texture getTexture() {
        if (clazz == Texture.class)
            return assetManager.get(filePath, Texture.class);
        else
            return null;
    }
}

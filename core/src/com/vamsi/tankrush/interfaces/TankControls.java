package com.vamsi.tankrush.interfaces;

/**
 * Created by Krishna on 17-Apr-15.
 */
public interface TankControls {
    void instruct(float xSpeed, float ySpeed);
    void resetInstruction();
    void fire(float x, float y);
}

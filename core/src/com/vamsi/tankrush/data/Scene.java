package com.vamsi.tankrush.data;

import com.google.gson.annotations.SerializedName;
import com.vamsi.tankrush.GameAssets;

/**
 * Created by Krishna on 17-Apr-15.
 */
public class Scene {
    @SerializedName("player_start_pos_x")
    public int playerStartPosX;

    @SerializedName("player_start_pos_y")
    public int playerStartPosY;

    public String background;

    //width in the number of backgrounds
    public int width;

    //height in the number of backgrounds
    public int height;

    public int getWidth() {
        return GameAssets.valueOf(background).getTexture().getWidth() * width;
    }

    public int getHeight() {
        return GameAssets.valueOf(background).getTexture().getHeight() * height;
    }

}

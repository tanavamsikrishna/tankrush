package com.vamsi.tankrush.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Krishna on 17-Apr-15.
 */
public class SceneData {
    private static SceneData instance;

    private int currentSceneId;
    public List<Scene> scenes;
    public List<Terrain> terrain;
    public List<Tank> tanks;

    @SerializedName("tank_movements")
    public List<TankMovement> tankMovements;

    @SerializedName("suicide_bombers")
    public List<SuicideBomber> suicideBombers;

    public static SceneData getInstance() {
        return instance;
    }

    public static void loadScene(int currentSceneId) {
        instance = new SceneData();
        instance.currentSceneId = currentSceneId;
        String filePath = "data/scene_" + currentSceneId + "_data.json";
        FileHandle handle = Gdx.files.internal(filePath);
        instance = new Gson().fromJson(handle.reader(), SceneData.class);
    }

    public static void dispose() {
        instance = null;
    }

    public Scene getScene() {
        return scenes.get(0);
    }
}

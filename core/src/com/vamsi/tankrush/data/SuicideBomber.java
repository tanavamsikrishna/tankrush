package com.vamsi.tankrush.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Krishna on 17-Apr-15.
 */
class SuicideBomber {
    String colour;

    int speed;

    @SerializedName("start_pos_x")
    int startPosX;

    @SerializedName("start_pos_y")
    int startPosY;
}

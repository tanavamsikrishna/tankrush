package com.vamsi.tankrush.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Krishna on 17-Apr-15.
 */
public class TankMovement {
    @SerializedName("tank_id")
    public int tankId;

    public int x;

    public int y;
}

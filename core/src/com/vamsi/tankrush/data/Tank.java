package com.vamsi.tankrush.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 17-Apr-15.
 */
public class Tank {
    private int id;

    String colour;

    int speed;

    @SerializedName("start_pos_x")
    int startPosX;

    @SerializedName("start_pos_y")
    int startPosY;

    @SerializedName("fire_interval")
    public int firingInterval;

    public List<TankMovement> getTankMovements() {
        List<TankMovement> tankMovements = new ArrayList<TankMovement>();
        for (TankMovement movement : SceneData.getInstance().tankMovements) {
            if (movement.tankId == id) {
                tankMovements.add(movement);
            }
        }
        return tankMovements;
    }
}

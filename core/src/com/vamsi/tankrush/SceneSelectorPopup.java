package com.vamsi.tankrush;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by Krishna on 07-May-15.
 */
class SceneSelectorPopup extends Group {
    public SceneSelectorPopup() {
        Image background = new Image(GameAssets.popup_background.getTexture());
        this.addActor(background);
    }
}

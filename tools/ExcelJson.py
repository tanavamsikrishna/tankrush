import json

import xlrd

scenes = [int(i) for i in open("scene_list.txt").readline().split(",")]
book = xlrd.open_workbook("Book1.xlsx")


def is_from_scene(scene_id, first_row, second_row, row_values):
    index_of_sheet_id = first_row.index("scene_id")
    return second_row[index_of_sheet_id] == "int" and scene_id == int(row_values[index_of_sheet_id])


for scene_id in scenes:
    json_book = {}
    for sheet in book.sheets():
        first_row = list(sheet.row_values(0))
        second_row = sheet.row_values(1)
        json_sheet = []
        for i in range(2, sheet.nrows):
            json_row = {}
            row_values = list(sheet.row_values(i))
            if not is_from_scene(scene_id, first_row, second_row, row_values):
                continue
            for j in range(len(row_values)):
                if first_row[j] == "scene_id":
                    continue
                if second_row[j] == "text":
                    json_row[first_row[j]] = str(row_values[j])
                elif second_row[j] == "int":
                    json_row[first_row[j]] = int(row_values[j])
                elif second_row[j] == "real":
                    json_row[first_row[j]] = float(row_values[j])
            json_sheet.append(json_row)
        json_book[sheet.name] = json_sheet
    json.dump(json_book, open("../android/assets/data/scene_" + str(scene_id) + "_data.json", "w"))
package com.vamsi.tankrush;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

class DesktopLauncher {
    public static void main(String[] arg) {
        new LwjglApplication(new TankRushGame(), "Tank Rush", 800, 480);
    }
}
